Part 1:

The idf value of 'insurance' is incorrect.

Part 2:
The 'D' value of out-degree is incorrect.

6)
 Node B has the largest in-degree and highest R value. Nodes A, C, and D have the same in-degrees and similar R values.
 Although in-degree and R values are very different, RageRank values reflect in-degrees (more in-links tend to generate larger R values)
 but take into account weights of individual citations/in-links.


If you have any questions regarding your grading, Please feel free to email me on- sr886@drexel.edu






